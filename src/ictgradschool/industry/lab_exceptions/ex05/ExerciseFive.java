package ictgradschool.industry.lab_exceptions.ex05;

import ictgradschool.Keyboard;

import java.util.StringTokenizer;

/**
 * TODO Write a program according to the Exercise Five guidelines in your lab handout.
 */
public class ExerciseFive {

    public void start() {
        // TODO Write the codes :)

        String userString;

        try{
            userString = askString();
        }
        catch (ExceedMaxStringLengthException e){
            System.out.println("Input is too long");
            start();

        }
        catch (InvalidWordException e){
            System.out.println("Invalid input");
            start();
        }


    }

    // TODO Write some methods to help you.

    public static String askString() throws ExceedMaxStringLengthException, InvalidWordException {
        System.out.println("Enter a string at most of 100 characters: ");

        String returnString = Keyboard.readInput();

        String single;

        StringTokenizer st = new StringTokenizer(returnString);

        while (st.hasMoreTokens()) {
                single = st.nextToken();
            if(Character.isDigit(single.charAt(0))){
                throw new InvalidWordException();
            }
        }

        if (returnString.length() > 100){
           throw new ExceedMaxStringLengthException();
       }

        printEntered(returnString);

        return returnString;
    }

    public static void printEntered(String returnString){

        System.out.println("You entered: ");
        StringTokenizer st = new StringTokenizer(returnString);

        while (st.hasMoreTokens()) {
            System.out.print(st.nextToken().charAt(0) + " ");
        }

    }
    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        new ExerciseFive().start();
    }
}
