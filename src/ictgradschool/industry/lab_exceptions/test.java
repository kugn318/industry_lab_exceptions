package ictgradschool.industry.lab_exceptions;

public class test {

    public static void main(String[] a){
        throwsClause10();
    }

    static void throwsClause10() {
        try {
            throws10(null);
            System.out.println("A");
        } catch (ArithmeticException e) {
            System.out.println(e);
        } finally {
            System.out.println("B");
        }
        System.out.println("C");
    }
    static void throws10(String numS) throws NullPointerException {
        if (numS == null) {
            throw new NullPointerException("Bad String");
        }
        System.out.println("D");
    }

}

